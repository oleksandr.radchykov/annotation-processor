package conf;

/**
 * @author Oleksandr Radchykov
 * @version <version>
 * @since <since>
 */
@Factory("Food")
public class Pizza {

	public static void main(String[] args) {
		Pizza pizza = Food.createPizza();
		System.out.println(Food.class);
		System.out.println(pizza.getClass());
	}

}
