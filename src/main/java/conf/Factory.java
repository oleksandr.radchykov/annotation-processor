package conf;

/**
 * @author Oleksandr Radchykov
 * @version <version>
 * @since <since>
 */
public @interface Factory {
	String value();
}
