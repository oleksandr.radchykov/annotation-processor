package conf;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Set;

/**
 * @author Oleksandr Radchykov
 * @version 0.0.1
 * @since 0.0.1
 */
@SupportedAnnotationTypes("conf.Factory")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class MyAnnotationProcessor extends AbstractProcessor {

	private ProcessingEnvironment processingEnvironment;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		this.processingEnvironment = processingEnv;
		super.init(processingEnv);
	}

	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		annotations.stream()
				.filter(annotation -> annotation.getSimpleName().toString().equals("Factory"))
				.forEach(factoryAnnotation ->
						roundEnv.getElementsAnnotatedWith(factoryAnnotation).forEach(element -> {
							generateFactory(((TypeElement) element));
						})
				);


		return true;
	}

	private void generateFactory(TypeElement clazz) {
		Name packageName = clazz.getEnclosingElement().getSimpleName();

		Factory factoryAnnotation = clazz.getAnnotation(Factory.class);
		String annotationValue = factoryAnnotation.value();

		Filer filer = processingEnvironment.getFiler();
		try {
			JavaFileObject sourceFile = filer.createSourceFile(packageName.toString() + "." + annotationValue);
			Writer writer = sourceFile.openWriter()
					.append("package " + packageName + ";\n")
					.append("public class " + annotationValue + " {\n")
					.append("public static " + clazz.getQualifiedName() + " create" + clazz.getSimpleName() + "() {\n")
					.append("return new " + clazz.getQualifiedName() + "();\n")
					.append("}\n")
					.append("}\n");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
